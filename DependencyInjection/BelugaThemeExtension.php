<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\DependencyInjection
 * @depends ContainerBuilder, FileLocator, Extension, Loader
 */

namespace Beluga\ThemeBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class BelugaThemeExtension extends Extension {

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container) {
                 
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml'); 
        

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
         
         foreach (array('themes', 'theme_active', 'path_patterns', 'cache_warming' , 'cookie' , 'config_root') as $key) {   
              $container->setParameter($this->getAlias().'.'.$key, $config[$key]);
        }
   
        
    }

}
