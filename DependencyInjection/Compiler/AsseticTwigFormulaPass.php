<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\DependencyInjection\Compiler
 * @depends ContainerBuilder, CompilerPassInterface, RuntimeException, Reference
 */

namespace Beluga\ThemeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\Reference;

class AsseticTwigFormulaPass implements CompilerPassInterface {

    /**
     * process
     * @param ContainerBuilder $container
     * @return type
     * @throws RuntimeException
     */
    public function process(ContainerBuilder $container) {
        if (!$container->hasParameter('beluga_theme.assetic_integration')) {
            return;
        }
        $bundles = $container->getParameter('kernel.bundles');
        if (!array_key_exists('AsseticBundle', $bundles)) {
            throw new RuntimeException(
            'You have enabled the "assetic_integration" option but the AsseticBundle is not registered'
            );
        }

        $loaderDef = $container->getDefinition('assetic.twig_formula_loader.real');

        $loaderDef->setClass('Beluga\ThemeBundle\Assetic\TwigFormulaLoader');
        $nbArguments = count($loaderDef->getArguments());
        // AsseticBundle 1.1.x does not have a logger definition, add it anyway
        // as it will be ignored by the older TwigFormulaLoader
        if ($nbArguments == 1) {
            $loaderDef->addArgument(new Reference('logger'));
        }
        $loaderDef->addArgument(new Reference('beluga_theme.manager'));
    }

}
