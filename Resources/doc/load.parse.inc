  /**
     * @todo theme:  \n name: ~  path: ~ css_path: ~ img_path: ~ js_path: ~
     * @param type $value
     * @return boolean
     * @example src/Beluga/ThemeBundle/Load/load.validate.inc How to use this function
     */
      
    public function validate($value) {

        if (in_array(array('theme', 'name', 'css_path', 'img_path', 'js_path'), $value)) {
            return $value;
        }
        return false;
    }