<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Helper 
 */

namespace Beluga\ThemeBundle\Helper;

/**
 * DeviceDetectionInterface
 */
interface DeviceDetectionInterface {

    /**
     * setUserAgent
     * @param type $userAgent
     */
    function setUserAgent($userAgent);

    /**
     * getType
     */
    function getType();
}
