<?php

namespace Beluga\ThemeBundle;

use Beluga\ThemeBundle\Helper\DeviceDetectionInterface;

/**
 * ActiveTheme
 * 
 * Contains the currently active theme and allows to change it.
 *
 * This is a service so we can inject it as reference to different parts of the application.
 */
class ActiveTheme {

    /**
     * $name
     * @var string
     */
    private $name;

    /**
     * $themes
     * @var array
     */
    private $themes;

    /**
     * $deviceDetection
     * @var DeviceDetectionInterface
     */
    private $deviceDetection;

    /**
     * __construct
     * 
     * @param string                          $name
     * @param array                           $themes
     * @param Helper\DeviceDetectionInterface $deviceDetection
     */
    public function __construct($name, array $themes = array(), DeviceDetectionInterface $deviceDetection = null) {


        $this->setThemes($themes);

        if ($name) {
            $this->active($name);
        }
        $this->deviceDetection = $deviceDetection;
    }

    /**
     * getDeviceDetection
     * @return DeviceDetectionInterface
     */
    public function getDeviceDetection() {
        return $this->deviceDetection;
    }

    /**
     * getThemes
     * @return array
     */
    public function getThemes() {
        return (array) $this->themes;
    }

    /**
     * setThemes
     * @param array $themes
     */
    public function setThemes(array $themes) {
        $this->themes = $themes;
    }

    /**
     * getName
     * @return type
     */
    public function getName() {
        return $this->name;
    }

    /**
     *  active
     * 
     * this method will activate the theme
     * @param type $name
     * @throws \InvalidArgumentException
     */
    public function active($name) {

        $list = array();
        foreach ($this->themes as $key => $value) {
            $list[] = $value['name'];
        }

        if (!in_array($name, $list)) {
            throw new \InvalidArgumentException(sprintf(
                    'The active 2. "%s" must be in the themes list (%s)', $name, implode(',', $list)
            ));
        }

        $this->name = $name;
    }

    /**
     * getDeviceType
     * @return string
     */
    public function getDeviceType() {
        if (!$this->deviceDetection) {
            return '';
        }

        return $this->deviceDetection->getType();
    }

}
