<?php

/**
 * @package Beluga\ThemeBundle\Command
 * @author Rodrigo manara@<rmanara@lightspeedresearch.com>
 * 
 */

namespace Beluga\ThemeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Beluga\ThemeBundle\Controller\AssetController;
use Symfony\Component\Console\Input\ArrayInput;

/**
 * AssetsThemesIntallCommand
 * 
 * <strong> only run on app/console </strong> 
 * 
 *  @example  src/Beluga/ThemeBundle/Resources/doc/command.theme.assets.inc 
 */
class AssetsThemesIntallCommand extends ContainerAwareCommand {

    /**
     * $load
     * this will hold  <strong>  $load </strong>  class comming from the service
     * 
     * @var object  
     */
    protected $load;

    /**
     * $finder
     * this will hold  <strong>  $finder  </strong> class comming from the service
     * 
     * @var object   
     */
    protected $finder;

    /**
     * $config
     * this will hold  <strong>  $config </strong> class comming from the service
     * 
     * @var object   
     */
    protected $config;

    /**
     * $file_manager
     * 
     * this will hold <strong>  $file_manager </strong>  class comming from the service
     * @var object 
     */
    protected $file_manager;
    protected $configRoot;

    /**
     * initialize
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);


        $this->load = $this->getContainer()->get('beluga_theme.load');
        $this->finder = $this->getContainer()->get('beluga_theme.finder');
        $this->config = $this->getContainer()->get('beluga_theme.confBuilder');
        $this->file_manager = $this->getContainer()->get('beluga_theme.fileManager');
        $this->configRoot = $this->getContainer()->getParameter('beluga_theme.config_root');
    }

    /**
     * configure
     */
    protected function configure() {
        $this
                ->setName('beluga:theme:assets')
                ->setDescription('Beluga Theme Assets Install')
                ->addArgument(
                        'install', InputArgument::OPTIONAL, 'do you want to update, this will only update the cache and rebuild the file'
                )
        ;
    }

    /**
     * execute
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $update = $input->getArgument('install');

        if ($update == 'install') {

            $this->install($input, $output);
        } elseif ($update == 'delete') {
            $this->remove($output);
        } else {
            $output->writeln("parameter missing : install or delete");
            $output->writeln("beluga:theme:assets");
        }
    }

    /**
     * install
     * 
     * @param InputInterface $input
     * @param OutputInterface $out
     */
    protected function install(InputInterface $input, OutputInterface $out) {

        $data = $this->load->load($this->configRoot);
        $config_yml = $this->config->build_path($data);
        $destination = $this->config->root() . '/../web/themes';

        $this->prep($out, $config_yml, $destination);

        /** run default assets * */
        $command = $this->getApplication()->find('assetic:dump');

        $arguments = array(
            'command' => 'assetic:dump'
//            ,'--watch' => true
        );

        $input = new ArrayInput($arguments);
        $command->run($input, $out);

    }

    /**
     * prep
     * 
     * @param OutputInterface $out
     * @param array $config_yml
     * @param type $destination
     */
    public function prep(OutputInterface $out, array $config_yml, $destination = null) {



        $this->file_manager->removeDir($destination);

        foreach ($config_yml as $key => $copy_from) {
            $copy_from_var = $this->config->root() . "/Resources/themes" . $copy_from['path'] . "/public";
            $copy_to = $destination . "/" . $key;


            $this->file_manager->createDir($copy_to);

            if ($this->file_manager->checkDir($copy_from_var) && $this->file_manager->checkDir($copy_to)) {

                //$this->file_manager->hardcopy();

                $this->installAssets($out, $copy_from_var, $copy_from, $destination);
            } else {
                if (!$this->file_manager->checkDir($copy_from_var)) {
                    $out->writeln("from --> wrong path: $copy_from_var  ===============");
                } elseif ($this->checkDir($copy_to)) {
                    $out->writeln("to -->wrong path: $copy_to  ===============");
                }
            }
        }
    }

    /**
     * remove
     * @param OutputInterface $out
     */
    public function remove(OutputInterface $out) {

        $config = $this->getContainer()->get('beluga_theme.confBuilder');
        $fileManager = $this->getContainer()->get('beluga_theme.fileManager');

        $destination = $config->root() . '/../web/themes/';
        $out->writeln("folder theme in Web has been deleted!!!");
        $fileManager->removeDir($destination);
    }

    /**
     * installAssets
     * 
     * @param OutputInterface $out
     * @param type $copy_from_var
     * @param type $var
     * @param type $copy_to
     */
    protected function installAssets(OutputInterface $out, $copy_from_var, $var, $copy_to) {

        /** call assets install * */
        $install = new AssetController($this->getContainer()->get('service_container'));

        $assets = array(
            'path' => $copy_to,
            
            'css_a' => $copy_from_var . '/css',
            'css_b' => $copy_to . $var['css'],
            'css_c' => 'themes' . $var['css'],
            
            'js_a' => $copy_from_var . '/js',
            'js_b' => $copy_to . $var['js'],
            'js_c' => 'themes' . $var['js'],
           
            'img_a' => $copy_from_var . '/img',
            'img_b' => $copy_to . $var['img'],
            'img_c' => 'themes' . $var['img'],
            
            'fonts_a' => $copy_from_var . '/fonts',
            'fonts_b' => $copy_to . $var['fonts'],
            'fonts_c' => 'themes'. $var['fonts'],
        );


        $install->SetAssets($assets);
    }

}