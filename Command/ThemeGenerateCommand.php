<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Command
 * @depends ContainerAwareCommand , InputArgument , InputInterface , OutputInterface
 */

namespace Beluga\ThemeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *  ThemeGenerateCommand
 * 
 *      <strong> only run on app/console </strong> 
 *  @todo lfkjsdhdfkjhsdfj sdklfjhsdlkfjhsdfs fsd;jfhsdkfjhsf sdl;fkjf
 *  @example src/Beluga/ThemeBundle/Resources/doc/command.theme.generate.inc 
 */
class ThemeGenerateCommand extends ContainerAwareCommand {

    /**
     * config
     * @var type 
     */
    protected $config;

    /**
     * filemanager
     * @var type 
     */
    protected $filemanager;

    /**
     * finder
     * @var type 
     */
    protected $finder;

    /**
     * load
     * @var type 
     */
    protected $load;

    /**
     * initialize
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);

        $this->config = $this->getContainer()->get('beluga_theme.confBuilder');
        $this->filemanager = $this->getContainer()->get('beluga_theme.fileManager');
        $this->finder = $this->getContainer()->get('beluga_theme.finder');
        $this->load = $this->getContainer()->get('beluga_theme.load');
    }

    /**
     *  configure
     */
    protected function configure() {
        $this
                ->setName('beluga:theme:install')
                ->setDescription('Beluga Theme  Install')
                ->addArgument(
                        'theme', InputArgument::REQUIRED, 'please, add the theme name'
                )

        ;
    }

    /**
     * execute
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) {

        $theme_name = $input->getArgument('theme');

        if (is_null($theme_name)) {
            $output->writeln("please add the name of the theme ...");
        } else {

            $this->install($output, $theme_name);
        }
    }

    /**
     * install
     * 
     * @param OutputInterface $out
     * @param type $name
     */
    protected function install(OutputInterface $out, $name) {



        $theme_name = trim($name);
        $file = $this->config->theme_folder_structure();
        $yml_config = $this->config->theme_file_config($theme_name);


        $root = $this->config->root() . "/Resources/themes/" . $theme_name;

        if ($this->filemanager->checkDir($root)) {

            $out->writeln("ops!!: theme \"{$theme_name}\" is already in use please choose other name.");
            $out->writeln("sugestion: \"{$theme_name}_1\" or \"{$theme_name}_2\"    ");

            exit();
        }

        ###     starting    ###
        $out->writeln("starting  generate directories ...");

        foreach ($file as $key => $public) {
            foreach ($public as $values) {
                $this->filemanager->createDir($root . DIRECTORY_SEPARATOR . key($public) . DIRECTORY_SEPARATOR . $values['css_path']);
                $out->writeln("generating {$values['css_path']} ...");
                $this->filemanager->createDir($root . DIRECTORY_SEPARATOR . key($public) . DIRECTORY_SEPARATOR . $values['js_path']);
                $out->writeln("generating {$values['js_path']} ...");
                $this->filemanager->createDir($root . DIRECTORY_SEPARATOR . key($public) . DIRECTORY_SEPARATOR . $values['img_path']);
                $out->writeln("generating {$values['img_path']} ...");
                $this->filemanager->createDir($root . DIRECTORY_SEPARATOR . key($public) . DIRECTORY_SEPARATOR . $values['fonts_path']);
                $out->writeln("generating {$values['fonts_path']} ...");
            }
        }



        $this->filemanager->createFile($root . DIRECTORY_SEPARATOR . "theme.html.twig");
        $out->writeln("generating theme.html.twig ...");
        $this->filemanager->createFile($root . DIRECTORY_SEPARATOR . "index.html.twig");
        $out->writeln("generating index.html.twig ...");
        $this->filemanager->createFile($root . DIRECTORY_SEPARATOR . "default.html.twig");
        $out->writeln("generating default.html.twig ...");
        sleep(1);
        $this->generateconfig($out);
    }

    /**
     * generateconfig
     * @param OutputInterface $out
     */
    protected function generateconfig(OutputInterface $out) {
        $out->writeln("starting service locator file ...");

        $out->writeln("starting write config file ...");
        sleep(1);

        $load_data = $this->finder->findDirectory();

        $out->writeln("loading ...");

        $data = $this->config->themesets($load_data);

        $out->writeln("<info>setting data ...</info>");
        $this->filemanager->generate_yml_file($data, $this->config->root() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'beluga' . DIRECTORY_SEPARATOR . 'beluga_theme.yml', 4);
        $out->writeln("generating beluga_theme.yml | config file ...");

        sleep(1);
    }

}
