ThemeBelugaBundle

this bundle is responsible to manage themes and files structured
to start this bundle please add it on the kernel 

add this into kernel in the app/AppKernel.php
new Beluga\ThemeBundle\BelugaThemeBundle()

The system will generate a file structure to create the Theme folder and a file 
called theme_bundle.

in that file you will have all details from the bundles

####################################
to generate a theme 

> php app/console beluga:theme:generate install new theme


#####################################
to generate a assets

> php app/console beluga:theme:assets install

####################################
to download it from composer

"beluga/theme-bundle" : "dev@master"