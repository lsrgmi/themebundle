<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Controller
 */

namespace Beluga\ThemeBundle\Controller;

use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\Asset\GlobAsset;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Filesystem;

/**
 * AssetController
 * this class is responsible for set the assets 
 */
class AssetController {

    /**
     * am = AssetManager
     * this will hold the object AssetManager
     * @var type 
     */
    protected $am;

    /**
     * container
     * @var type 
     */
    protected $container;

    /**
     * router
     * @var type 
     */
    protected $router;

    /**
     *  __construct
     * @param Container $container
     */
    public function __construct(Container $container) {

        $this->container = $container;
        $this->am = $this->container->get('assetic.asset_manager');
        $this->router = $this->container->get('beluga_theme.routing');
    }

    /**
     * setAssets 
     * this will create the function and generate a default js file on the web folder.
     * 
     * @param array $config
     */
    public function SetAssets(array $config) {

       
   
        $this->createfile(
                array(
                array('file' => $config['js_b'] . '/javascript.js', 'dir' => $config['js_b']),
                array('file' => $config['css_b'] . '/stylesheets.css', 'dir' => $config['css_b']),
                array('dir' => $config['fonts_b']),
            array('dir' => $config['img_b'])
                )
                , new Filesystem());
                 
        
        
        /** set js * */
        $this->js($config);
        /** set css */
        $this->css($config);
        /**      image   * */
        $this->image($config);
        /** fonts * */
        $this->fonts($config);
    }

    /**
     * js
     * @param array $config
     */
    protected function js(array $config) {
        $scripts = new AssetCollection(
                array(new GlobAsset($config['js_a'] . '/*'), new FileAsset($config['js_b'] . '/javascript.js'))
        );

        //, new FileAsset($config['js_b'] . '/javascript.js')

        $id = 'jquery_' . rand(0, 999999999);
        $scripts->setTargetPath($config['js_c'] . '/javascript.js');
        $this->am->set($id, $scripts);
    }

    /**
     *  css
     * @param array $config
     */
    protected function css(array $config) {

        $styles = new AssetCollection(
                array(new GlobAsset($config['css_a'] . '/*'), new FileAsset($config['css_b'] . '/stylesheets.css')
                )
        );

        $styles->setTargetPath($config['css_c'] . '/stylesheets.css');
        $this->am->set('base_css' . rand(0, 999999999), $styles);
    }

    /**
     *  images
     * @param array $config
     */
    protected function image(array $config) {

        $fm = new Filesystem();


        $fm->mirror($config['img_a'], $config['img_b']);
    }

    /**
     *  fonts
     * @param array $config
     */
    protected function fonts(array $config) {
        $fm = new Filesystem();
        $fm->mirror($config['fonts_a'], $config['fonts_b']);
    }

    /**
     * create file
     * @param array $files
     * @param Filesystem $fileSystem
     */
    public function createfile(array $files, Filesystem $fileSystem) {

        if (is_array($files)) {
            foreach ($files as $file) {

                if (isset($file['dir'])) {
                    $fileSystem->mkdir($file['dir']);
                }

                if (isset($file['file'])) {
                    $fileSystem->touch($file['file']);
                }
            }
        }
    }

}
