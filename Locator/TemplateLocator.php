<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Locator
 * @depends BaseTemplateLocator , FileLocatorInterface , TemplateReferenceInterface
 */


namespace Beluga\ThemeBundle\Locator;

use Symfony\Bundle\FrameworkBundle\Templating\Loader\TemplateLocator as BaseTemplateLocator;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\Templating\TemplateReferenceInterface;

/**
 * TemplateLocator
 * 
 *  this will locate all files
 */
class TemplateLocator extends BaseTemplateLocator {

    /**
     * 
     * @param FileLocatorInterface $locator
     * @param type $cacheDir
     */
    public function __construct(FileLocatorInterface $locator, $cacheDir = null) {
        parent::__construct($locator, $cacheDir);
    }

    /**
     * locate
     * 
     * @param TemplateReferenceInterface $template
     * @param type $currentPath
     * @param type $first
     * @return type
     * @throws \InvalidArgumentException
     */
    public function locate($template, $currentPath = null, $first = true) {
        if (!$template instanceof TemplateReferenceInterface) {
            throw new \InvalidArgumentException('The template must be an instance of TemplateReferenceInterface.');
        }

        $templateParameters = $template->all();
        
        
        
        $templateBundle = array_key_exists('bundle', $templateParameters) ? $templateParameters['bundle'] : null;
        

        if (isset($this->categoryBundle) and  null !== $this->categoryBundle && $templateBundle === $this->defaultBundle /* Override only defaultBundle templates */
        ) {
            /* Try our category bundle first */
            $categoryTemplate = clone $template;
            $categoryTemplate->set('bundle', $this->categoryBundle);

            try {
                return parent::locate($categoryTemplate, $currentPath, $first);
            } catch (\InvalidArgumentException $exception) {
                /* Just fall through to default mechanism */
            }
        }

        return parent::locate($template, $currentPath, $first);
    }

}
