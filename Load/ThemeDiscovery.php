<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Load
 * @depends Finder
 */

namespace Beluga\ThemeBundle\Load;

use Symfony\Component\Finder\Finder;

/**
 * ThemeDiscovery
 * this class is used to  find all info.yml inside the themes folder
 */
class ThemeDiscovery {

    /**
     * $path_root
     * @var type 
     */
    private $path_root;

    /**
     * $files
     * @var type 
     */
    private $files = array();

    /**
     * $file_name
     * @var type 
     */
    private $file_name;

    /**
     * __construct
     * 
     * @param type $kernel_root
     */
    public function __construct($kernel_root) {
        $this->path_root = $kernel_root . "/Resources/themes/";
        //$this->file_name = 'info.yml';
    }

    /**
     * find
     * 
     * this class will look throught the theme folders  to find a  file called info.yml
     * @return array
     */
    public function find() {

        $finder = new Finder();
        $finder->files()
                ->in($this->path_root)
                ->name($this->file_name);

        foreach ($finder as $file) {
            array_push($this->files, array('file' => $this->file_name, "path" => $file->getRealpath()));
        }

        return $this->files;
    }
    /**
     * FindDirectory
     * this will look for all directories on the AppResources
     * 
     * @return array
     */
    public function findDirectory(){
         $finder = new Finder();
          $finder
                 ->in($this->path_root)
                 ->directories()->depth(0);
        
          $array_dir = array();
          
         foreach($finder as $dir){
             array_push($array_dir ,  array('path' => $dir->getRelativePathname()));
         }
        
         return $array_dir;
         
    }
    
    /**
     * supports
     * 
     * @param type $resource
     * @param type $type
     * @return type
     */
    public function supports($resource, $type = null) {
        return is_string($resource) && 'yml' === pathinfo(
                        $resource, PATHINFO_EXTENSION
        );
    }

}
