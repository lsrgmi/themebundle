<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Twig
 * @depends Controller 
 */

namespace Beluga\ThemeBundle\Twig;

use Symfony\Bundle\FrameworkBundle\Controller;

/**
 *  AppExtension
 * this method is resposible to import html into the twig extension
 */
class AppExtension extends \Twig_Extension {

    /**
     * getFilters
     * 
     * @return type
     */
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('in_html', array($this, 'include_html')),
        );
    }

    /**
     * include_html
     * 
     * @param type $file_path
     * @return type
     */
    public function include_html($file_path) {
        $container = new Controller();
        $path = $container->get('kernel')->locateResource('@BelugaThemeBudle/Resource/views');

        return file_get_contents($path . DIRECTORY_SEPARATOR . $file_path);
    }

    /**
     * getName
     * 
     * @return string
     */
    public function getName() {
        return 'app_extension';
    }

}
