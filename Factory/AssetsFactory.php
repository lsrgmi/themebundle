<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Factory
 * @depends AssetInterface, AssetFactory , WorkerInterface 
 */

namespace Beluga\ThemeBundle\Factory;

use Assetic\Asset\AssetInterface;
use Assetic\Factory\Worker\WorkerInterface;
use Assetic\Factory\AssetFactory;

class AssetsFactory implements WorkerInterface {

    public function process(AssetInterface $asset, AssetFactory $factory) {

        $targetUrl = $asset->getTargetPath();

        if ($targetUrl && '/' != $targetUrl[0] && 0 !== strpos($targetUrl, '_controller/')) {
            $asset->setTargetPath('_controller/' . $targetUrl);
        }
         
        return $asset;
    }

}
