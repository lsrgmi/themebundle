<?php

namespace Test\Beluga\ThemeBundle\Load;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2015-02-25 at 16:15:37.
 */
class LoadTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Load
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->object = new Load;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @covers Beluga\ThemeBundle\Load\Load::load
     * @todo   Implement testLoad().
     */
    public function testLoad() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers Beluga\ThemeBundle\Load\Load::validate
     * @todo   Implement testValidate().
     */
    public function testValidate() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

}
