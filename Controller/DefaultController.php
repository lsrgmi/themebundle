<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Controller
 */

namespace Beluga\ThemeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * DefaultController
 * this class is a demo
 * 
 */
class DefaultController extends Controller {

    /**
     * $response
     * @var type 
     */
    protected $response;

    /**
     * indexAction
     *  @example src/Beluga/ThemeBundle/Doc/default.controller.inc 
     */
    public function indexAction($name) {
        return $this->render('index.html.twig', array('name' => $name));
    }

}
