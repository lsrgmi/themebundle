<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch>
 * @package Beluga\ThemeBundle\Utils
 */

namespace Beluga\ThemeBundle\Utils;

/**
 * ConfigurationBuilder
 * this class will build and configure the file tree 
 * 
 * <strong>please make sure the file in:</strong>
 *      "@Symfony_root_app app/config/beluga/beluga_theme.yml is there "
 * 
 */
class ConfigurationBuilder {

    /**
     * path_root
     * is used to holde the path root set on the service
     * @var string path_root
     */
    protected $path_root;

    /**
     * $path
     * is used to hold the path set on the service
     * @var string path
     */
    private $path;

    /**
     * $file_confg
     * @var string file_config
     */
    private $file_confg;

    /**
     * __construct
     * @param string $root
     * @param string $path
     */
    public function __construct($root, $path = null) {
        $this->path_root = $root;
        $this->path = $path;
    }

    /**
     * 
     * @param array $array
     * @return array $set
     */
    public function build_path(array $array) {
        $set = array();


        foreach ($array as $key => $config) {
            foreach ($config['themes'] as $key => $data) {
                $set[$data['name']]['path'] = DIRECTORY_SEPARATOR . $data['name'];
                $set[$data['name']]['css'] = DIRECTORY_SEPARATOR . $data['assets']['css'];
                $set[$data['name']]['js'] = DIRECTORY_SEPARATOR . $data['assets']['js'];
                $set[$data['name']]['img'] = DIRECTORY_SEPARATOR . $data['assets']['img'];
                $set[$data['name']]['fonts'] = DIRECTORY_SEPARATOR . $data['assets']['fonts'];
            }
        }

        return $set;
    }

    /**
     * @todos organize the array from the yml file and configure to be build in a new format
     * @param array $array
     * @return array
     */
    public function build_assets_path(array $array) {


        $set = array();

        foreach ($array as $config) {
            $set['twig'][] = array($config['theme']['path'] => $config['theme']['name']);
            $set['asset_js'][] = $config['theme']['js_path'] . "/*";
            $set['assets_css'][] = $config['theme']['css_path'] . "/*";
        }
        return $set;
    }

    /**
     * @class themesets
     * this will set theme config
     * @param array $data
     */
    public function themesets(array $data = array()) {

        $array = array();

        foreach ($data as $key => $value) {
            $array['beluga_theme']['themes'][$value['path']] = 
                    array('name' => $value['path'], 
                        'assets' => array(
                            'css' => $value['path'] . '/css',
                            'js' => $value['path'] . '/js',
                            'fonts' => $value['path'] . '/fonts',
                            'img' => $value['path'] . '/img',
            ));
        }
        $array['beluga_theme']['theme_active'] = $data[0]['path'];
        $array['beluga_theme']['config_root'] = "%kernel.root_dir%/config/beluga/beluga_theme.yml";

        return $array;
    }

    /**
     * Assets
     * This will organize the data from the yml file to build anew yml file 
     * @param array $set
     * @return type
     */
    public function assets(array $set) {
        return array(
            'twig' => array(
                'paths' => $set['twig']
            ),
            'assetic' => array(
                'assets' => array(
                    'jquery_and_ui' => array(
                        'input' => $set['asset_js'],
                        'output' => 'js/scripts.js'
                    ),
                    'css' => array(
                        'input' => $set['assets_css'],
                        'output' => 'css/style_theme.css'
                    )
                )
            )
        );
    }

    /**
     * 
     * @param type $name
     * @return type
     */
    public function theme_folder_structure() {
        return array('theme' =>
            array('public' =>
                array(
                    'css_path' => "/css",
                    'img_path' => "/img",
                    'js_path' => "/js",
                    'fonts_path' => "/fonts",
                )
            )
        );
    }

    /**
     * theme_file_config
     * @param type $name
     * @return type
     */
    public function theme_file_config($name) {
        return array('theme' => array(
                'name' => "{$name}",
                'path' => "/{$name}",
                'css_path' => "/{$name}/css",
                'img_path' => "/{$name}/img",
                'js_path' => "/{$name}/js",
                'fonts_path' => "/{$name}/fonts",
            )
        );
    }

    /**
     * 
     * @return type
     */
    public function root() {
        return $this->path_root;
    }

}
