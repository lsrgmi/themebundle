<?php

/**
 * @package Beluga\ThemeBundle\Assetic
 * @author Rodrigo manara@<rmanara@lightspeedresearch.com>
 * 
 */

namespace Beluga\ThemeBundle\Assetic;

use Assetic\Factory\Loader\FormulaLoaderInterface;
use Assetic\Factory\Resource\ResourceInterface;
use Psr\Log\LoggerInterface;
use Liip\ThemeBundle\ActiveTheme;
use Assetic\Extension\Twig\TwigFormulaLoader as BaseTwigFormulaLoader;

/**
 * TwigFormulaLoader
 */
class TwigFormulaLoader extends BaseTwigFormulaLoader {

    /**
     * $activeTheme
     * @var type 
     */
    private $activeTheme;

    /**
     * $twig
     * @var type 
     */
    private $twig;

    /**
     * $logger
     * @var type 
     */
    private $logger;

    /**
     * __construct
     * @param \Twig_Environment $twig
     * @param LoggerInterface $logger
     * @param ActiveTheme $activeTheme
     */
    public function __construct(
    \Twig_Environment $twig, LoggerInterface $logger = null, ActiveTheme $activeTheme = null
    ) {
        parent::__construct($twig, $logger);
        $this->activeTheme = $activeTheme;
        $this->twig = $twig;
        $this->logger = $logger;
    }

    /**
     *  load
     * @param ResourceInterface $resource
     * @return type
     */
    public function load(ResourceInterface $resource) {

        $formulae = array();
        $failureTemplates = array();
        $successTemplates = array();
        foreach ($this->activeTheme->getThemes() as $theme) {
            $this->activeTheme->active($theme);
            try {
                // determine if the template has any errors
                $this->twig->tokenize($resource->getContent());
                // delegate the formula loading to the parent
                $formulae = array_merge($formulae, parent::load($resource));
                $successTemplates[(string) $resource] = true;
            } catch (\Exception $e) {
                $failureTemplates[(string) $resource] = $e->getMessage();
            }
        }
        if ($this->logger) {
            foreach ($failureTemplates as $failureTemplate => $exceptionMessage) {
                if (isset($successTemplates[$failureTemplate])) {
                    continue;
                }
                $this->logger->warning(sprintf(
                                'The template "%s" contains an error: "%s"', $resource, $exceptionMessage
                ));
            }
        }
        return $formulae;
    }

}
