<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\DependencyInjection\Compiler
 * @depends ContainerBuilder, DirectoryResourceDefinition, CompilerPassInterface, LogicException
 */

namespace Beluga\ThemeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Bundle\AsseticBundle\DependencyInjection\DirectoryResourceDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Exception\LogicException;

/**
 * TemplateResourcesPass
 * 
 */
class TemplateResourcesPass implements CompilerPassInterface {

    public function process(ContainerBuilder $container) {

        if (empty($container->getParameter('beluga_theme.themes'))) {
            return;
        } elseif (!$container->hasDefinition('assetic.asset_manager')) {
            return;
        }

        $engines = $container->getParameter('templating.engines');

        // bundle and kernel resources
        $bundles = $container->getParameter('kernel.bundles');


        $asseticBundles = $container->getParameterBag()->resolveValue($container->getParameter('assetic.bundles'));
        foreach ($asseticBundles as $bundleName) {

            $rc = new \ReflectionClass($bundles[$bundleName]);

            foreach ($engines as $engine) {
                $this->setBundleDirectoryResources($container, $engine, dirname($rc->getFileName()), $bundleName);
            }
        }

        foreach ($engines as $engine) {
            $this->setThemesDirectory($container, $engine);
        }

        foreach ($engines as $engine) {
            $this->setAppDirectoryResources($container, $engine);
        }
    }

    /**
     * setBundleDirectoryResources
     * @param ContainerBuilder $container
     * @param type $engine
     * @param type $bundleDirName
     * @param type $bundleName
     * @throws LogicException
     */
    protected function setBundleDirectoryResources(ContainerBuilder $container, $engine, $bundleDirName, $bundleName) {
        if (!$container->hasDefinition('assetic.' . $engine . '_directory_resource.' . $bundleName)) {
            throw new LogicException('The BelugaThemeBundle must be registered after the AsseticBundle in the application Kernel.');
        }

        $resources = $container->getDefinition('assetic.' . $engine . '_directory_resource.' . $bundleName)->getArgument(0);

        $themes = $container->getParameter('beluga_theme.themes');



        foreach ($themes as $key => $theme) {

            $resources[] = new DirectoryResourceDefinition(
                    $bundleName, $engine, array(
                $container->getParameter('kernel.root_dir') . '/Resources/' . $bundleName . '/themes/' . $theme['name'],
                $bundleDirName . '/Resources/themes/' . $theme['name'],
                    )
            );
        }

        $container->getDefinition('assetic.' . $engine . '_directory_resource.' . $bundleName)->replaceArgument(0, $resources);
    }

    /**
     * setAppDirectoryResources
     * @param ContainerBuilder $container
     * @param type $engine
     */
    protected function setAppDirectoryResources(ContainerBuilder $container, $engine) {
        if (!$container->hasDefinition('assetic.' . $engine . '_directory_resource.kernel')) {
            
        }

        $themes = $container->getParameter('beluga_theme.themes');

        foreach ($themes as $key => $theme) {
            $themes[$theme['name']] = $container->getParameter('kernel.root_dir') . '/Resources/themes/' . $theme['name'];
        }
        $themes[] = $container->getParameter('kernel.root_dir') . '/Resources/views';

        $container->setDefinition(
                'assetic.' . $engine . '_directory_resource.kernel', new DirectoryResourceDefinition('', $engine, $themes)
        );
    }
    /**
     * setThemesDirectory
     * @param ContainerBuilder $container
     * @param type $engine
     * @throws LogicException
     */
    protected function setThemesDirectory(ContainerBuilder $container, $engine) {
        if (!$container->hasDefinition('assetic.' . $engine . '_directory_resource.kernel')) {
            throw new LogicException('The BelugaThemeBundle must be registered after the AsseticBundle in the application Kernel.');
        }

        $themes = $container->getParameter('beluga_theme.themes');

        foreach ($themes as $key => $theme) {
            $themes[$theme['name']] = $container->getParameter('kernel.root_dir') . '/Resources/themes/' . $theme['name'] . "/";
        }


        $container->setDefinition(
                'assetic.' . $engine . '_directory_resource.kernel', new DirectoryResourceDefinition('', $engine, $themes)
        );
    }

}
