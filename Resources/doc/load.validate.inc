<?php

/**
 * <code>
 * <p>
 * ex:
 * <ul>
 * <li> Theme </li>
 * <ul>
 * <il> name </li>
 * <il> path <p> this value must be on the validate otherwise will not validate the file </p> </li>
 * <li> css_path </li>
 * <li> img_path</li>
 * <li> js_path </li>
 * </ul>
 * </ul> : : ~ path: ~ css_path: ~ img_path: ~ js_path: ~
 * </p>
 * </code>

  /**
 * @method file_get_contents
 * @todo parse will load the file and will parse to  ynl file to array
 * @param $values string
 * @return array and will return all data from the yml file 
 * @example src/Beluga/ThemeBundle/Load/load.inc How to use this function
 * @example load.inc This example is in the "examples" subdirectory
 */
function parse($values) {
    $yaml = new Parser();
    try {
        $value = $yaml->parse(file_get_contents($values));
        $value = $this->validate($value);
    } catch (ParseException $e) {
        printf("Unable to parse the YAML string: %s", $e->getMessage());
    }

    return $value;
}

/**
 * Here you can call the namespace
 */
use Beluga\ThemeBundle\Load;

$parse = load();

$parse->parse();
?>