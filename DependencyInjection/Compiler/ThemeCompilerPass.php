<?php


/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\DependencyInjection\Compiler
 * @depends ContainerBuilder, CompilerPassInterface
 */

namespace Beluga\ThemeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * ThemeCompilerPass
 * 
 */
class ThemeCompilerPass implements CompilerPassInterface {
    
    /**
     *  process
     * 
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container) {
        $container->setAlias('templating.locator', 'beluga_theme.templating_locator');

        $container->setAlias('templating.cache_warmer.template_paths', 'beluga_theme.templating.cache_warmer.template_paths');
        $container->getDefinition('beluga_theme.templating.cache_warmer.template_paths')
                ->replaceArgument(2, null);
    }

}
