<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Load
 * @depends Parser , ParseException
 */


namespace Beluga\ThemeBundle\Load;

use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * Load
 * 
 */
class Load {

    /**
     * set_config
     * 
     * @var type 
     */
    protected $set_config;
    /**
     * set_validation
     * 
     * @var type 
     */
    protected $set_validation;

    /**
     *  load
     * 
     * @todo here you will load the path to parse the yml file form the theme folder
     * @param type $data
     * @return array
     */
    public function load($data = array()) {
        
         
        if (is_array($data)) {
            $parse = array();
            foreach ($data as $values) {
                $value = $this->parse($values['path']);
                array_push($parse, $value);
            }
        } else {
            $parse = $this->parse($data);
        }

        return $parse;
    }

    /**
     *  parse
     * 
     * @method file_get_contents
     * @todo parse will load the file and will parse to  ynl file to array
     * @param $values string
     * @return array and will return all data from the yml file 
     * @example src/Beluga/ThemeBundle/Doc/load.parse.inc How to use this function
     * @source 2 5 this will parse the method 
     */
    protected function parse($values) {
        $yaml = new Parser();
        try {
            $value = $yaml->parse(file_get_contents($values));
            //$value = $this->validate($value);
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }

        return $value;
    }

    /**
     * validate
     * 
     * @todo theme:  \n name: ~  path: ~ css_path: ~ img_path: ~ js_path: ~
     * @param type $value
     * @return boolean
     * @example src/Beluga/ThemeBundle/Doc/load.validate.inc How to use this function
     */
    protected function validate($value) {

        if (in_array($this->set_validation, $value)) {
            return $value;
        }
        return false;
    }

    /**
     *  set_validation
     * 
     * @param type $array
     *  @example src/Beluga/ThemeBundle/Doc/load.validate.inc How to use this function
     * @source 2 1
     */
    public function set_validation($array = array()) {
        $this->set_validation = $array;
    }

    /**
     * get_validation
     */
    public function get_validation() {
        return $this->set_validation;
    }

}
