<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\Locator
 * @depends BaseTemplateLocator , FileLocatorInterface , TemplateReferenceInterface
 */

namespace Beluga\ThemeBundle\Routing;

use Assetic\Factory\LazyAssetManager;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Finder\Finder;

/**
 * RouterCollection
 */
class RouterCollection extends Loader {

    /**
     * $loaded
     * @var type 
     */
    private $loaded = false;

    /**
     * am
     * @var type 
     */
    protected $am;

    /**
     * varValues
     * @var type 
     */
    private $varValues;

    /**
     * root
     * @var type 
     */
    private $root;

    /**
     *  __construct
     * 
     * @param LazyAssetManager $am
     * @param type $root
     * @param array $varValues
     */
    public function __construct(LazyAssetManager $am, $root, array $varValues = array()) {
        $this->am = $am;
        $this->varValues = $varValues;
        $this->root = $root . "/../web/themes";
    }

    /**
     * load
     * @param type $resource
     * @param type $type
     * @return RouteCollection
     */
    public function load($resource, $type = null) {
        $collection = new RouteCollection();

        $resource = '@AcmeDemoBundle/Resources/config/import_routing.yml';
        $type = 'yaml';

        $importedRoutes = $this->import($resource, $type);

        $collection->addCollection($importedRoutes);

        return $collection;
    }

    /**
     *  supports
     * 
     * @param type $resource
     * @param type $type
     * @return type
     */
    public function supports($resource, $type = null) {
        return $type === 'advanced_extra';
    }

    /**
     * preloadload
     * @param type $routes
     */
    public function preloadload($routes) {


        foreach ($this->am->getNames() as $name) {

            $finder = new Finder();

            $finder->files()
                    ->in($this->root)
                    ->name('*.js')
                    ->name('*.css')
            ;

            $i = 0;
            foreach ($finder as $file) {
                //$this->router->load($config['js_c'] . DIRECTORY_SEPARATOR . $file->getRelativePathname(), $id, $i);
                $this->addRouter($routes, $name, $file->getRelativePathname(), $i);
                $i++;
            }
        }
    }

    /**
     * addRouter
     * @param RouteCollection $routes
     * @param type $name
     * @param type $path
     * @param type $pos
     */
    protected function addRouter(RouteCollection $routes, $name, $path, $pos) {

        $defaults = array(
            '_controller' => 'assetic.controller:render',
            'name' => $name,
            'pos' => $pos,
        );
        $requirements = array();


        if ($format = pathinfo($file, PATHINFO_EXTENSION)) {
            $defaults['_format'] = $format;
        }

        $route = '_assetic_' . $name;
        if (null !== $pos) {
            $route .= '_' . $pos;
        }



        $routes->add($route, new Route($file, $defaults, $requirements));
    }

}
