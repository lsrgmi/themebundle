<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\EventListener
 * @depends GetResponseEvent, Load , ThemeDiscovery , ConfigurationBuilder ,  FilesystemLoader , Container
 */

namespace Beluga\ThemeBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Beluga\ThemeBundle\Load\Load;
use Beluga\ThemeBundle\Load\ThemeDiscovery;
use Beluga\ThemeBundle\Utils\ConfigurationBuilder;
use Symfony\Bundle\TwigBundle\Loader\FilesystemLoader;
use \Symfony\Component\DependencyInjection\Container;

class RequestViewListener {

    /**
     * $finder
     * @var type 
     */
    protected $finder;

    /**
     * $config
     * @var type 
     */
    protected $config;

    /**
     * $load
     * @var type 
     */
    protected $load;

    /**
     * $twig
     * @var type 
     */
    protected $twig;

    /**
     * $request
     * @var type 
     */
    protected $request;

    /**
     * $container
     * @var type 
     */
    protected $container;

    /**
     * __construct
     * 
     * @param FilesystemLoader $twig
     * @param Container $container
     */
    public function __construct(FilesystemLoader $twig, Container $container) {
        $this->twig = $twig;
        $this->container = $container;
    }

    /**
     * onKernelRequest
     * this call you will set the alias to a path set via theme controller
     * 
     * @param GetResponseEvent $event
     * @return GetResponseEvent
     */
    public function onKernelRequest(GetResponseEvent $event) {

        $paran = $this->container->getParameter('beluga_theme.themes');
        $root = $this->container->get("beluga_theme.confBuilder")->root();

        foreach ($paran as $key => $value) {

            $this->twig->addPath($root . "/Resources/themes/" . $value['name'], $key);
        }

        return $event;
    }

}
