<?php

namespace Beluga\ThemeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Beluga\ThemeBundle\DependencyInjection\Compiler\ThemeCompilerPass;
use Beluga\ThemeBundle\DependencyInjection\Compiler\TemplateResourcesPass;
use Beluga\ThemeBundle\DependencyInjection\Compiler\AsseticTwigFormulaPass;
use Beluga\ThemeBundle\Utils\FileManager;

/**
 * BelugaThemBundle
 * this class is reposible to start the themebundle to add parts into the main container
 * 
 * @example src/Beluga/ThemeBundle/Resources/doc/bundle.inc 
 */
class BelugaThemeBundle extends Bundle {

    /**
     * boot
     * will start the sistem and will generate a file called beluga_theme.yml
     */
    public function boot() {
        parent::boot();
        
        $this->setConfigFile();
    }

    /**
     *  build
     * Build will start build the container
     * 
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container) {
        parent::build($container);
        
        
        $container->addCompilerPass(new ThemeCompilerPass());
        $container->addCompilerPass(new TemplateResourcesPass());
        $container->addCompilerPass(new AsseticTwigFormulaPass());
    }

    protected function setConfigFile() {
        
        $build = new FileManager();
        $path = $this->container->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . 'config/beluga';
        $filename = $path . DIRECTORY_SEPARATOR . 'beluga_theme.yml';
 
        
        if (!$build->checkDir($path)) {
            $build->createDir($path);
            $build->createFile($filename);
        }
        
    }

}
