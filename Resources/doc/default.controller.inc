public function indexAction($name) {

        $theme = $this->container->get('beluga_theme.controller');
        $theme->setTheme($this->get('request')->get('theme')) ;


        return $this->render('BelugaThemeBundle::Default/index.html.twig', array('name' => $name, 'theme' => $theme->getTheme()));
}