<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\DependencyInjection
 * @depends TreeBuilder , ConfigurationInterface
 */

namespace Beluga\ThemeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface {

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('beluga_theme', 'array');

        $rootNode
               ->children()
                    ->scalarNode('config_root')
                         ->defaultNull()  
                    ->end()
                    ->arrayNode('themes')
                        ->prototype('array')
                                 ->children()
                                    ->scalarNode('name')->end()
                                    ->arrayNode('assets')
                                                ->children()
                                                    ->scalarNode('css')->end()
                                                    ->scalarNode('js')->end()
                                                    ->scalarNode('fonts')->end()
                                                    ->scalarNode('img')->end()
                                                ->end() 
                                    ->end() 
                                 ->end()   
                        ->end()
                    ->end()
                    ->arrayNode('path_patterns')
                      ->addDefaultsIfNotSet()
                      ->children()
                          ->arrayNode('app_resource')
                              ->useAttributeAsKey('path')
                              ->prototype('scalar')->end()
                          ->end()
                          ->arrayNode('bundle_resource')
                              ->useAttributeAsKey('path')
                              ->prototype('scalar')->end()
                          ->end()
                          ->arrayNode('bundle_resource_dir')
                              ->useAttributeAsKey('path')
                              ->prototype('scalar')->end()
                          ->end()
                      ->end()
                  ->end()
                    ->scalarNode('theme_active')->defaultNull()->end()
                       ->arrayNode('cookie')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('name')->cannotBeEmpty()->end()
                        ->scalarNode('lifetime')->defaultValue(31536000)->end()
                        ->scalarNode('path')->defaultValue('/')->end()
                        ->scalarNode('domain')->defaultValue('')->end()
                        ->booleanNode('secure')->defaultFalse()->end()
                        ->booleanNode('http_only')->defaultFalse()->end()
                    ->end()
                ->end()
                ->scalarNode('autodetect_theme')->defaultFalse()->end()
                ->booleanNode('cache_warming')->defaultTrue()->end()
                ->booleanNode('load_controllers')->defaultTrue()->end()
             
                ->end();
        
        return $treeBuilder;
    }

}
