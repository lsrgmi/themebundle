<?php

/**
 * @author Rodrigo Manara<rmanara@lightspeedresearch.com>
 * @package Beluga\ThemeBundle\EventListener
 * @depends GetResponseEvent, FilterResponseEvent , HttpKernelInterface , HttpKernelInterface ,  Cookie , Request
 */

namespace Beluga\ThemeBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;

use Beluga\ThemeBundle\Helper\DeviceDetectionInterface;
use Beluga\ThemeBundle\ActiveTheme;

/**
 * ThemeRequestListener
 * Listens to the request and changes the active theme based on a cookie.
 *
 */
class ThemeRequestListener
{
    /**
     * activeTheme
     * @var ActiveTheme
     */
    protected $activeTheme;

    /**
     * cookieOptions
     * @var array
     */
    protected $cookieOptions;

    /**
     * autoDetect
     * @var DeviceDetectionInterface
     */
    protected $autoDetect;

    /**
     * @var string
     */
    protected $newTheme;

    /**
     * @param ActiveTheme              $activeTheme
     * @param array                    $cookieOptions The options of the cookie we look for the theme to set
     * @param DeviceDetectionInterface $autoDetect    If to auto detect the theme based on the user agent
     */
    public function __construct(ActiveTheme $activeTheme, array $cookieOptions = null, DeviceDetectionInterface $autoDetect = null)
    {
        $this->activeTheme = $activeTheme;
        $this->autoDetect = $autoDetect;
        $this->cookieOptions = $cookieOptions;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
            $cookieValue = null;
            if (null !== $this->cookieOptions) {
                $cookieValue = $event->getRequest()->cookies->get($this->cookieOptions['name']);
            }

            if (!$cookieValue && $this->autoDetect instanceof DeviceDetectionInterface) {
                $cookieValue = $this->getThemeType($event->getRequest());
            }

            if ($cookieValue && $cookieValue !== $this->activeTheme->getName()
                && in_array($cookieValue, $this->activeTheme->getThemes())
            ) {
                $this->activeTheme->active($cookieValue);
                // store into cookie
                if ($this->cookieOptions) {
                    $this->newTheme = $cookieValue;
                }
            }
        }
    }

    /**
     * Given the Request return the device type.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return string the user agent type
     */
    private function getThemeType(Request $request)
    {
        $userAgent = $request->server->get('HTTP_USER_AGENT');
        $this->autoDetect->setUserAgent($userAgent);

        return $this->autoDetect->getType();
    }


    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
            // store into the cookie only if the controller did not already change the value
            if ($this->newTheme == $this->activeTheme->getName()) {
                $cookie = new Cookie(
                    $this->cookieOptions['name'],
                    $this->newTheme,
                    time() + $this->cookieOptions['lifetime'],
                    $this->cookieOptions['path'],
                    $this->cookieOptions['domain'],
                    (Boolean)$this->cookieOptions['secure'],
                    (Boolean)$this->cookieOptions['http_only']
                );
                $event->getResponse()->headers->setCookie($cookie);
            }
        }
    }
}
